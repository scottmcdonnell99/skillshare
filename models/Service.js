const mongoose = require('mongoose');

const bookmarkSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

const reviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  rating: {
    type: Number,
    required: true,
  },
  comment: {
    type: String,
    required: true,
  },
});

const serviceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
  },
  category: {
    type: String,
    required: true,
    minlength: 5,
  },
  location: {
    type: String,
    required: true,
    minlength: 5,
  },
  price: {
    type: Number,
    required: true,
  },
  phone: {
    type: Number,
    required: true,
    minlength: 10,
  },
  datePosted: {
    type: Date,
    default: Date.now,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  image: { type: String, default: '../assets/defaultService.png' },
  reviews: [reviewSchema],
  averageRating: { type: Number, default: 0 },
  views: {
    type: Number,
    default: 0,
  },
  bookmarks: [bookmarkSchema]
});

serviceSchema.set('toJSON', {
  transform: (returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;

    returnedObject.datePosted = returnedObject.datePosted.toLocaleString();
  },
});

const Service = mongoose.model('Service', serviceSchema);

module.exports = Service;
