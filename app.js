const mongoose = require('mongoose');
const express = require('express')
const app = express();
const config = require('./utils/config')
const middleware = require('./utils/middleware')
const servicesRouter = require('./controllers/services');
const loginRouter = require('./controllers/login');
const usersRouter = require ('./controllers/users')
const registerRouter = require ('./controllers/register')

require('dotenv').config()

const cors = require('cors')

mongoose.set('strictQuery',false)

console.log('connecting to', config.MONGODB_URI)

mongoose.connect(config.MONGODB_URI)
    .then(() => {
        console.log('connected to MongoDB')
    })
    .catch((error) => {
        console.error('error connecting to MongoDB:', error.message)
    })

app.use(cors())
app.use(express.json());

app.use('/api/login', loginRouter)
app.use('/api/users', usersRouter)
app.use('/api/services', servicesRouter)
app.use('/api/register', registerRouter)

app.get('/api/api-key', (req, res) => {
    const apiKey = config.googleMapsApiKey;
    res.json({ key: apiKey });
  });

app.use(middleware.unknownEndpoint)
app.use(middleware.errorHandler)

module.exports = app