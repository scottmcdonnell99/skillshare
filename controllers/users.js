const bcrypt = require('bcrypt');
const usersRouter = require('express').Router();
const User = require('../models/user');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken')
const { authenticateToken } = require('../utils/middleware')

const getTokenFrom = request => {
  const authorization = request.get('authorization')
  if (authorization && authorization.startsWith('Bearer ')) {
      return authorization.replace('Bearer ', '')
  }
  return null
}

usersRouter.post('/', async (request, response) => {
  try {
    const { username, name, password } = request.body;

    // Perform password validation if needed

    const saltRounds = 10;
    const passwordHash = await bcrypt.hash(password, saltRounds);

    const user = new User({
      username,
      name,
      passwordHash,
    });

    const savedUser = await user.save();

    // Exclude passwordHash from the response
    const userResponse = {
      _id: savedUser._id,
      username: savedUser.username,
      name: savedUser.name,
    };

    response.status(201).json(userResponse);
  } catch (error) {
    console.error('Error during user registration:', error.message);
    response.status(500).json({ error: 'Internal Server Error' });
  }
});

usersRouter.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Validate if id is a valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({ error: 'Invalid user ID format' });
      
    }

    const user = await User.findById(id).populate('services', {
      name: 1,
      category: 1,
      location: 1,
      price: 1,
      phone: 1,
      datePosted: 1,
    }).populate('bookmarks', {
      name: 1,
      category: 1,
      location: 1,
      price: 1,
      phone: 1,
      datePosted: 1,
    });

    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    const sanitizedUser = {
      _id: user._id,
      username: user.username,
      name: user.name,
      services: user.services.map(service => ({
        _id: service._id,
        name: service.name,
        category: service.category,
        location: service.location,
        price: service.price,
        phone: service.phone,
        datePosted: service.datePosted,
      })),
      bookmarks: user.bookmarks.map(bookmark => ({
        _id: bookmark._id,
        name: bookmark.name,
        category: bookmark.category,
        location: bookmark.location,
        price: bookmark.price,
        phone: bookmark.phone,
        datePosted: bookmark.datePosted,
      })),
    };
    
    res.status(200).json(sanitizedUser);
  } catch (error) {
    console.error('Error retrieving user in the userRouters function:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Route to get all users
usersRouter.get('/', async (request, response) => {
  try {
    const users = await User.find({}).populate('services', {
      name: 1,
      category: 1,
      location: 1,
      price: 1,
      phone: 1,
      datePosted: 1,
    });

    // Exclude sensitive information from the response
    const sanitizedUsers = users.map(user => ({
      _id: user._id,
      username: user.username,
      name: user.name,
      services: user.services,
      bookmarks: user.bookmarks,
    }));

    response.json(sanitizedUsers);
  } catch (error) {
    console.error('Error getting users:', error.message);
    response.status(500).json({ error: 'Internal Server Error' });
  }
});

// Route to get all bookmarks for a user
usersRouter.get('/bookmarks/:id', authenticateToken, async (req, res) => {
  try {
    const token = getTokenFrom(req);

    const decodedToken = jwt.verify(token, process.env.SECRET);

    if (!decodedToken.id) {
      return res.status(401).json({
        error: 'Token invalid',
      });
    }

    const id = decodedToken.id;

    // Validate if id is a valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({ error: 'Invalid user ID format' });
    }

    // Find the user by ID and populate the 'bookmarks' field
    const user = await User.findById(id).populate('bookmarks', {
      name: 1,
      category: 1,
      location: 1,
      price: 1,
      phone: 1,
      datePosted: 1,
    });

    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    // Extract and return the bookmarks
    const bookmarks = user.bookmarks.map((bookmark) => ({
      _id: bookmark._id,
      name: bookmark.name,
      category: bookmark.category,
      location: bookmark.location,
      price: bookmark.price,
      phone: bookmark.phone,
      datePosted: bookmark.datePosted,
    }));

    res.status(200).json(bookmarks);
  } catch (error) {
    console.error('Error retrieving bookmarks for user:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


module.exports = usersRouter;
