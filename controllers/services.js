const express = require('express');
const servicesRouter = express.Router();
const Service = require('../models/Service');
const User = require('../models/User');
const jwt = require('jsonwebtoken')
const { authenticateToken } = require('../utils/middleware')

const getTokenFrom = request => {
    const authorization = request.get('authorization')
    if (authorization && authorization.startsWith('Bearer ')) {
        return authorization.replace('Bearer ', '')
    }
    return null
}

// Route to get all services
servicesRouter.get('/', async (req, res) => {
  try {
    const allServices = await Service.find().populate('userId', 'username name');
    res.status(200).json(allServices);
  } catch (error) {
    console.error('Error retrieving services:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

servicesRouter.get('/my-listings', authenticateToken, async (req, res) => {
  try {
    const userId = req.user.id;
    const userServices = await Service.find({ userId });
    res.status(200).json(userServices);
  } catch (error) {
    console.error('Error retrieving user listings:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Route to get a specific service by ID
servicesRouter.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const service = await Service.findById(id).populate('userId', 'username name');

    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }

    service.views += 1;

    res.status(200).json(service);
  } catch (error) {
    console.error('Error retrieving service:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Route to create a new service
servicesRouter.post('/', async (req, res) => {
  try {
    const token = getTokenFrom(req);
    const decodedToken = jwt.verify(token, process.env.SECRET);

    if (!decodedToken.id) {
      return res.status(401).json({
        error: 'Token invalid',
      });
    }

    const { name, category, location, phone, price } = req.body;

    const user = await User.findById(decodedToken.id);
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }
    const newService = new Service({
      name,
      category,
      location,
      phone,
      price,
      userId: user._id,
    });

    const savedService = await newService.save();
    user.services = user.services.concat(savedService._id);
    await user.save();
    res.status(201).json(savedService);
  } catch (error) {
    if (error.name === 'JsonWebTokenError') {
      return res.status(401).json({
        error: 'Invalid token',
      });
    } else if (error.name === 'ValidationError') {
      res.status(400).json({ error: error.message });
    } else {
      console.error('Error creating service:', error.message);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
})

servicesRouter.put('/:id', authenticateToken, async (req, res) => {
  try {
    const token = getTokenFrom(req);
    const decodedToken = jwt.verify(token, process.env.SECRET);

    if (!decodedToken.id) {
      return res.status(401).json({
        error: 'Token invalid',
      });
    }
    const user = await User.findById(decodedToken.id);

    // Use req.params.id to get the service ID from the route parameters
    const id = req.params.id;

    const existingService = await Service.findById(id);

    if (!existingService) {
      return res.status(404).json({ error: 'Service not found' });
    }

    // Ensure that the user updating the service is the owner
    if (existingService.userId.toString() !== user._id.toString()) {
      return res.status(403).json({ error: 'Permission denied' });
    }

    const updatedService = await Service.findByIdAndUpdate(id, req.body, { new: true });

    res.status(200).json(updatedService);
  } catch (error) {
    console.error('Error updating service:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

servicesRouter.delete('/:id', authenticateToken, async (req, res) => {
  try {
    const { id } = req.params;
    const deletedService = await Service.findByIdAndDelete(id);

    if (!deletedService) {
      return res.status(404).json({ error: 'Service not found' });
    }

    res.status(200).json(deletedService); // Send the deleted service data in the response
  } catch (error) {
    console.error('Error deleting service:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Route to create a new review for a service
servicesRouter.post('/:id/reviews', async (req, res) => {
  try {
    const token = getTokenFrom(req);
    const decodedToken = jwt.verify(token, process.env.SECRET);

    if (!decodedToken.id) {
      return res.status(401).json({
        error: 'Token invalid',
      });
    }

    const { rating, comment } = req.body;
    const userId = decodedToken.id;
    const serviceId = req.params.id;

    // Fetch the service
    const service = await Service.findById(serviceId);

    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }

    // Create a new review object with the user details
    const newReview = {
      user: userId,
      rating,
      comment,
    };

    // Add the review to the service
    service.reviews.push(newReview);

    // Update the average rating
    const totalRating = service.reviews.reduce((sum, review) => sum + review.rating, 0);
    service.averageRating = totalRating / service.reviews.length;

    await service.save();

    res.status(201).json(service);
  } catch (error) {
    if (error.name === 'JsonWebTokenError') {
      return res.status(401).json({
        error: 'Invalid token',
      });
    } else {
      console.error('Error adding review:', error.message);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
});


// Fetch reviews for a service
servicesRouter.get('/:id/reviews', async (req, res) => {
  try {
    const serviceId = req.params.id;
    const service = await Service.findById(serviceId).populate('reviews.user', 'username name');

    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }

    res.status(200).json(service.reviews);
  } catch (error) {
    console.error('Error fetching reviews:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

servicesRouter.post('/:id/increment-views', async (req, res) => {
  try {
    const { id } = req.params;
    const service = await Service.findById(id);

    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }

    // Increment the views count
    service.views += 1;
    await service.save();

    res.status(200).json({ views: service.views });
  } catch (error) {
    console.error('Error incrementing views:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

servicesRouter.post('/:id/bookmark', authenticateToken, async (req, res) => {
  try {
    const serviceId = req.params.id;
    const token = getTokenFrom(req);

    const decodedToken = jwt.verify(token, process.env.SECRET);

    if (!decodedToken.id) {
      return res.status(401).json({
        error: 'Token invalid',
      });
    }

    const userId = decodedToken.id;
    const service = await Service.findById(serviceId);

    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }

    // Check if user is already bookmarked
    const isBookmarked = service.bookmarks.some(bookmark => bookmark._id.toString() === userId.toString());
    if (isBookmarked) {
      // If user is already bookmarked, remove the bookmark from the service
      service.bookmarks = service.bookmarks.filter(bookmark => bookmark._id.toString() !== userId.toString());
      
      // Update the user's bookmarks array by removing the serviceId
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { $pull: { bookmarks: service._id } },
        { new: true }
      );
    } else {
      // If user is not bookmarked, add the bookmark to the service
      service.bookmarks.push({ _id: userId });
      
    // Update the user's bookmarks array by adding the serviceId
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { $addToSet: { bookmarks: service.toObject()} },
        { new: true }
      ).populate('bookmarks'); // Populate the bookmarks field in the user object
    }

    await service.save();

    res.status(200).json({ bookmarks: service.bookmarks });
  } catch (error) {
    console.error('Error bookmarking service:', error.message);

    // Handle specific error types
    if (error.name === 'JsonWebTokenError') {
      return res.status(401).json({ error: 'Invalid token' });
    } else if (error.name === 'CastError') {
      return res.status(400).json({ error: 'Invalid service ID format' });
    }

    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Backend route to get bookmark status
servicesRouter.get('/:id/bookmark-status', authenticateToken, async (req, res) => {
  try {
    const serviceId = req.params.id;

    const token = getTokenFrom(req);

    const decodedToken = jwt.verify(token, process.env.SECRET);

    if (!decodedToken.id) {
      return res.status(401).json({
        error: 'Token invalid',
      });
    }

    const userId = decodedToken.id;

    const service = await Service.findById(serviceId);

    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }

    const isBookmarked = service.bookmarks.some(bookmark => bookmark._id.toString() === userId.toString());

    res.status(200).json({ isBookmarked });
  } catch (error) {
    console.error('Error fetching bookmark status:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});



module.exports = servicesRouter;

