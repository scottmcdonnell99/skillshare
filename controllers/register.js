const bcrypt = require('bcrypt');
const registerRouter = require('express').Router();
const User = require('../models/user');

registerRouter.post('/', async (request, response) => {
  try {
    const { username, name, password } = request.body;

    // Perform password validation if needed

    const saltRounds = 10;
    const passwordHash = await bcrypt.hash(password, saltRounds);

    const user = new User({
      username,
      name,
      passwordHash,
    });

    const savedUser = await user.save();

    // Exclude passwordHash from the response
    const userResponse = {
      _id: savedUser._id,
      username: savedUser.username,
      name: savedUser.name,
    };

    response.status(201).json(userResponse);
  } catch (error) {
    console.error('Error during user registration:', error.message);
    response.status(500).json({ error: 'Internal Server Error' });
  }
});

module.exports = registerRouter;
