// EditServiceForm.jsx
import React, { useState } from 'react';
import '../App.css'


const EditServiceForm = ({ service, onCancel, onUpdate }) => {
  const [updatedService, setUpdatedService] = useState({ ...service });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUpdatedService((prevService) => ({ ...prevService, [name]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onUpdate(updatedService);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" name="name" value={updatedService.name} onChange={handleChange} />
      </label>
      <label>
        Category:
        <input type="text" name="category" value={updatedService.category} onChange={handleChange} />
      </label>
      <label>
        Location:
        <input type="text" name="location" value={updatedService.location} onChange={handleChange} />
      </label>
      <label>
        Phone:
        <input type="text" name="phone" value={updatedService.phone} onChange={handleChange} />
      </label>
      <label>
        Price:
        <input type="text" name="price" value={updatedService.price} onChange={handleChange} />
      </label>
      <button type="button" onClick={onCancel}>
        Cancel
      </button>
      <button type="submit">Update</button>
    </form>
  );
};

export default EditServiceForm;
