import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as emptyStar } from '@fortawesome/free-regular-svg-icons';
import { faHeart as solidHeart } from '@fortawesome/free-solid-svg-icons';
import { faHeart as emptyHeart } from '@fortawesome/free-regular-svg-icons';
import serviceService from '../services/services';

const Service = ({ service }) => {
  const [isBookmarked, setIsBookmarked] = useState(false);
  const hasValidRating = typeof service.averageRating === 'number' && service.averageRating >= 0 && service.averageRating <= 5;
  const roundedRating = hasValidRating ? Math.round(service.averageRating) : 0;
  const emptyStarsCount = 5 - roundedRating;
  const starsArray = Array.from({ length: roundedRating });
  const emptyStarsArray = Array.from({ length: emptyStarsCount });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const bookmarksData = await serviceService.getBookmarksForService(service._id);
        setIsBookmarked(bookmarksData.isBookmarked);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [service._id]);

  const toggleBookmark = async () => {
    try {
      await serviceService.toggleBookmark(service._id);
      setIsBookmarked((prevIsBookmarked) => !prevIsBookmarked);

      // Perform additional actions based on the response, if needed
    } catch (error) {
      // Handle the error, show an error message, etc.
      console.error('Error toggling bookmark:', error.message);
    }
  };

  return (
    <div className="service-card-content">
      <Link to={`/services/${service._id}`} className="service-card-title">
        <h2>{service.name}</h2>
      </Link>
      <p className="service-card-subtitle">{service.category}</p>
      <p className="service-card-location">{service.location}</p>
      {hasValidRating ? (
        <div className="service-card-rating">
          {starsArray.map((_, index) => (
            <FontAwesomeIcon key={index} icon={faStar} style={{ color: 'gold' }} />
          ))}
          {emptyStarsArray.map((_, index) => (
            <FontAwesomeIcon key={index} icon={emptyStar} style={{ color: '#555' }} />
          ))}
        </div>
      ) : (
        <p>No rating</p>
      )}
      <p className="service-card-price">{service.price}</p>
      <button
        className="service-card-icon"
        onClick={toggleBookmark}
        style={{ 
          color: isBookmarked ? 'red' : 'inherit',
          backgroundColor: 'transparent', // Set background color to transparent
          border: 'none', // Remove button border
          outline: 'none', // Remove button outline
          fontSize: '20px', // Adjust the size as needed
        }}
        service={service}
      >
        <FontAwesomeIcon icon={isBookmarked ? solidHeart : emptyHeart} />
      </button>
    </div>
  );
};

export default Service;
