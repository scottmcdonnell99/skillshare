import React, { useState, useEffect } from 'react';
import userService from '../services/users';
import User from './User'; // Make sure to import your User component
import '../App.css'


const AllUsers = () => {
  const [users, setUsers] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    userService
      .getAll()
      .then((allUsers) => setUsers(allUsers))
      .catch((error) => console.error('Error fetching users:', error));
  }, []);

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  // Log data to check if it's changing as expected
  console.log('Search Query:', searchQuery);

  // Filter users based on the search query
  const filteredUsers = users.filter((user) =>
    user.username.toLowerCase().includes(searchQuery.toLowerCase())
  );

  console.log('Filtered Users:', filteredUsers);

  return (
    <div>
      <h2>All Users</h2>
      <input
        type="text"
        placeholder="Search users..."
        value={searchQuery}
        onChange={handleSearchChange}
      />
      <ul>
        {filteredUsers.map((user) => (
          <User key={user._id} user={user} />
        ))}
      </ul>
    </div>
  );
};

export default AllUsers;
