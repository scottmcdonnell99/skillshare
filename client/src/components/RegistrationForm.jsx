// RegistrationForm.js
import React, { useState } from 'react';
import registerService from '../services/register';
import '../App.css'

const RegistrationForm = () => {
  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const [formVisible, setFormVisible] = useState(true);

  const handleRegistration = async (event) => {
    event.preventDefault();

    try {
      // Call the register function from the registration service
      const registrationResult = await registerService({
        username,
        name,
        password,
      });

      console.log('Registration successful:', registrationResult);

      // Clear form fields, display success message, and hide the form
      setUsername('');
      setName('');
      setPassword('');
      setRegistrationSuccess(true);
      setFormVisible(false);
    } catch (error) {
      // Handle registration error (e.g., display an error message)
      console.error('Registration error:', error.message);
      setRegistrationSuccess(false);
    }
  };

  return (
    <div>
      <h2>Registration</h2>
      {registrationSuccess && <p style={{ color: 'green' }}>Registration successful!</p>}
      {formVisible && (
        <form>
          <label>
            Username:
            <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
          </label>
          <br />
          <label>
            Name:
            <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
          </label>
          <br />
          <label>
            Password:
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
          </label>
          <br />
          <br />
          <button type="button" onClick={handleRegistration}>
            Register
          </button>
        </form>
      )}
    </div>
  );
};

export default RegistrationForm;
