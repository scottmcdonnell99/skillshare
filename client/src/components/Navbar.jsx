import React from 'react';
import DropdownMenu from './DropdownMenu';
import RegistrationForm from './RegistrationForm';
import LoginForm from './LoginForm';
import Togglable from './Togglable';
import ServiceForm from './ServiceForm';
import { Link } from 'react-router-dom';

const NavBar = ({
  user,
  handleLogout,
  handleLogin,
  handleAddService,
  handleServiceChange,
  newService,
  setUsername,
  setPassword,
  username,
  password,
  serviceFormRef,
}) => {
  return (
    <div className="navbar-container">
      <div className="nav-left">
        <button className='home-button'>
          <Link to={`/`}>SkillShare</Link>
        </button>
        {!user && (
          <div>
            <Togglable buttonLabel="Register" buttonClassName='nav-button'>
              <RegistrationForm
                username={username}
                password={password}
                handleUsernameChange={({ target }) => setUsername(target.value)}
                handlePasswordChange={({ target }) => setPassword(target.value)}
                handleSubmit={handleLogin}
              />
            </Togglable>
            <Togglable buttonLabel="Login" buttonClassName='nav-button'>
              <LoginForm
                username={username}
                password={password}
                handleUsernameChange={({ target }) => setUsername(target.value)}
                handlePasswordChange={({ target }) => setPassword(target.value)}
                handleSubmit={handleLogin}
              />
            </Togglable>
          </div>
        )}
      </div>
      <div>
        {user && (
          <div>
            <Togglable buttonLabel="+ Place Listing" buttonClassName='place-listing-button' ref={serviceFormRef}>
              <ServiceForm
                createService={handleAddService}
                onChange={handleServiceChange}
                values={newService}
              />
            </Togglable>
            <DropdownMenu user={user} handleLogout={handleLogout} />
          </div>
        )}
      </div>
    </div>
  );
};

export default NavBar;
