// AllServices.js

import React, { useState, useEffect } from 'react';
import serviceService from '../services/services';
import Service from './Service';
import ServiceMap from './ServiceMap';
import { Link } from 'react-router-dom';
import SearchInput from './SearchInput';
import '../css/AllServices.css';

const ITEMS_PER_PAGE = 4; // Number of services to display per page

const AllServices = ( { user } ) => {
  const [services, setServices] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [currentPage, setCurrentPage] = useState(1);

  console.log(user)
  
  useEffect(() => {
    serviceService
      .getAll()
      .then((allServices) => setServices(allServices))
      .catch((error) => console.error('Error fetching services:', error));
  }, []);

  const filteredServices = services.filter(
    (service) =>
      service.name.toLowerCase().includes(searchTerm.toLowerCase())
  );

  // Calculate the range of services to display for the current page
  const indexOfLastService = currentPage * ITEMS_PER_PAGE;
  const indexOfFirstService = indexOfLastService - ITEMS_PER_PAGE;
  const currentServices = filteredServices.slice(indexOfFirstService, indexOfLastService);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
    setCurrentPage(1); // Reset to the first page when the search term changes
  };


  return (
    <div className="all-services-container">
      <div className="services-list-container">
        <SearchInput
          placeholder="Search services..."
          value={searchTerm}
          onChange={handleSearch}
        />
        {currentServices.map((service) => (
          <div key={service._id} className="service-card">
            <Service service={service} />
          </div>
        ))}
        <div className="pagination">
          {Array.from({ length: Math.ceil(filteredServices.length / ITEMS_PER_PAGE) }, (_, index) => (
            <button key={index + 1} onClick={() => paginate(index + 1)}>
              {index + 1}
            </button>
          ))}
        </div>
      </div>

      <div className="service-map-container">
        <ServiceMap services={currentServices} />
      </div>
    </div>
  );
};

export default AllServices;
