import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

const ServiceForm = ({ createService }) => {
  const [newService, setNewService] = useState({
    name: '',
    category: '',
    location: '',
    phone: '',
    price: '',
  });

  const addServiceHandler = (event) => {
    event.preventDefault();
    createService(newService);
    setNewService({
      name: '',
      category: '',
      location: '',
      phone: '',
      price: '',
    });
  };

  return (
    <div>
      <form className='service-form' onSubmit={addServiceHandler}>
        <div>
          <label htmlFor="name"></label>
          <input
            id="name"
            value={newService.name}
            onChange={(event) => setNewService({ ...newService, name: event.target.value })}
            placeholder="Service Name"
          />
        </div>
        <div>
          <label htmlFor="category"></label>
          <input
            id="category"
            value={newService.category}
            onChange={(event) => setNewService({ ...newService, category: event.target.value })}
            placeholder="Service Category"
          />
        </div>
        <div>
          <label htmlFor="location"></label>
          <input
            id="location"
            value={newService.location}
            onChange={(event) => setNewService({ ...newService, location: event.target.value })}
            placeholder="Service Location"
          />
        </div>
        <div>
          <label htmlFor="phone"></label>
          <input
            id="phone"
            value={newService.phone}
            onChange={(event) => setNewService({ ...newService, phone: event.target.value })}
            placeholder="Service Phone"
          />
        </div>
        <div>
          <label htmlFor="price"></label>
          <input
            id="price"
            value={newService.price}
            onChange={(event) => setNewService({ ...newService, price: event.target.value })}
            placeholder="Service Price"
          />
        </div>
        <button id="post-button" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
};

export default ServiceForm;
