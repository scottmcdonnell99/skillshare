// Navigation.jsx
import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css'

const Navigation = () => {
  const navigate = useNavigate();

  const handleGoBack = () => {
    navigate(-1); // Go back one step in the history
  };

  return (
    <button onClick={handleGoBack}>Go Back</button>
  );
};

export default Navigation;
