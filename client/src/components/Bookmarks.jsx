import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import userService from '../services/users';
import serviceService from '../services/services';
import { Link } from 'react-router-dom';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import '../App.css'


const MyBookmarks = () => {
  const [bookmarks, setBookmarks] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const { userId } = useParams();

  useEffect(() => {
    const fetchBookmarks = async () => {
      try {
        setLoading(true);
        setError(null);

        const loggedUserJSON = window.localStorage.getItem('loggedServiceAppUser');
        if (!loggedUserJSON) {
          // Redirect the user to the login page or perform necessary actions
          // For example, you can use React Router to navigate to the login page
          return;
        }

        const loggedUser = JSON.parse(loggedUserJSON);
        userService.setToken(loggedUser.token);

        // Use the serviceService functions to fetch bookmark details
        const bookmarkIds = await userService.getUserBookmarks(userId);

        const bookmarkDetailsPromises = bookmarkIds.map(async (bookmarkId) => {
          try {
            // Fetch the details for each bookmarkId
            const bookmarkDetails = await serviceService.getServiceById(bookmarkId._id);
            return bookmarkDetails;
          } catch (error) {
            // Handle any errors fetching individual bookmark details
            console.error('Error fetching bookmark details:', error.message);
            return null; // or handle it in a way that fits your application
          }
        });

        // Use Promise.all to wait for all bookmark details to be fetched
        const bookmarkDetails = await Promise.all(bookmarkDetailsPromises);

        // Filter out any null values (failed to fetch details)
        const validBookmarkDetails = bookmarkDetails.filter((details) => details !== null);

        setBookmarks(validBookmarkDetails);
      } catch (error) {
        console.error('Error fetching bookmarks:', error.message);
        setError('Error fetching bookmarks. Please try again.');
      } finally {
        setLoading(false);
      }
    };

    fetchBookmarks();
  }, [userId]);

  if (loading) {
    return (
      <div className="loading-container">
        <faSpinner className="loading-icon" />
      </div>
    );
  }

  if (error) {
    return <p>{error}</p>;
  }

  return (
    <div>
      <h2>My Bookmarks</h2>
      {bookmarks.length === 0 ? (
        <p>No bookmarks found.</p>
      ) : (
        <ul>
          {bookmarks.map((bookmark) => (
            <li key={bookmark._id}>
              <div>
                <strong>Name:</strong> {bookmark.name}
              </div>
              <div>
                <strong>Category:</strong> {bookmark.category}
              </div>
              <div>
                <strong>Location:</strong> {bookmark.location}
              </div>
              <div>
                <strong>Phone:</strong> {bookmark.phone}
              </div>
              <div>
                <strong>Posted by:</strong> {bookmark.postedBy}
              </div>
              <div>
                <strong>Average Rating:</strong> {bookmark.averageRating}
              </div>
              <div>
                <strong>Views:</strong> {bookmark.views}
              </div>
              <div>
                {/* Add a link to the service using its ID */}
                <Link to={`/services/${bookmark._id}`}>View Listing</Link>
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default MyBookmarks;
