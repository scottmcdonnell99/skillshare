import React, { useEffect, useState } from 'react';
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api';
import serviceService from '../services/services';
import mapService from '../services/map';
import Service from './Service';
import '../App.css';

const ServiceMap = ({ services }) => {
  const mapStyles = {
    height: '1000px',
    width: '100%',
  };

  const defaultCenter = { lat: 53.349805, lng: -6.26031 };
  const defaultZoom = 12;
  const [apiKey, setApiKey] = useState(null);
  const [markers, setMarkers] = useState([]);
  const [selectedMarker, setSelectedMarker] = useState(null);
  const [userLocation, setUserLocation] = useState(null);
  const [userMarker, setUserMarker] = useState(null);
  const [isGoogleMapsLoaded, setIsGoogleMapsLoaded] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchApiKey = async () => {
      try {
        const response = await mapService.fetchApiKey();
        setApiKey(response);
        console.log('API Key:', response);
      } catch (error) {
        console.error('Error fetching API key:', error);
      } finally {
        setIsLoading(false);
      }
    };

    if (apiKey === null) {
      fetchApiKey();
    }
  }, [apiKey]);

  useEffect(() => {
    const fetchMarkers = async () => {
      const updatedMarkers = [];

      for (const service of services) {
        try {
          const coordinates = await serviceService.geocodeAddress(service.location);

          if (coordinates && coordinates.lat && coordinates.lng) {
            updatedMarkers.push({
              id: service._id,
              position: { lat: coordinates.lat, lng: coordinates.lng },
              serviceInfo: service,
            });
          }
        } catch (error) {
          console.error(`Error geocoding address for service ${service._id}:`, error);
        }
      }

      setMarkers(updatedMarkers);
    };

    fetchMarkers();
  }, [services]);

  useEffect(() => {
    const fetchUserLocation = async () => {
      if (navigator.geolocation) {
        try {
          const position = await new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(resolve, reject);
          });

          setUserLocation({
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          });

          setUserMarker({
            position: {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            },
          });
        } catch (error) {
          console.error('Error getting user location:', error);
        }
      }
    };

    fetchUserLocation();
  }, []);

  const handleLoadMap = () => {
    setIsGoogleMapsLoaded(true);
  };

  return (
    <div>
      {isLoading && <div>Loading API Key...</div>}
      {!isLoading && (
        <LoadScript
          googleMapsApiKey={apiKey}
          onLoad={handleLoadMap}
        >
          {isGoogleMapsLoaded ? (
            <GoogleMap 
              mapContainerStyle={mapStyles} 
              zoom={defaultZoom} 
              center={userLocation || defaultCenter} 
              disableDefaultUI
              options={{
                styles: [
                  {
                    featureType: 'poi',
                    elementType: 'labels',
                    stylers: [{ visibility: 'off' }],
                  },
                ],
                mapTypeControl: false,
                streetViewControl: false,
                keyboardShortcuts: false,
                zoomControl: false,
                zoomControlOptions: {
                  position: window.google.maps.ControlPosition.RIGHT_BOTTOM,
                },
                fullscreenControl: false,
                fullscreenControlOptions: {
                  position: window.google.maps.ControlPosition.RIGHT_BOTTOM,
                },
              }}
            >
              {markers.map((marker) => (
                <Marker
                  key={marker.id}
                  position={marker.position}
                  onClick={() => setSelectedMarker(marker)}
                />
              ))}
              {userMarker && (
                <Marker
                  position={userMarker.position}
                  icon={{
                    url: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                    scaledSize: new window.google.maps.Size(30, 30),
                    anchor: { x: 15, y: 15 },
                  }}
                />
              )}
              {selectedMarker && (
                <InfoWindow
                  position={selectedMarker.position}
                  onCloseClick={() => setSelectedMarker(null)}
                >
                  <Service service={selectedMarker.serviceInfo} />
                </InfoWindow>
              )}
            </GoogleMap>
          ) : (
            <div>Loading Map...</div>
          )}
        </LoadScript>
      )}
    </div>
  );
};

export default ServiceMap;
