import { useState, useEffect } from 'react'
import EditServiceForm from '../components/EditServiceForm'
import serviceService from '../services/services'
import '../App.css'

const MyProfile = () => {
    const [editingService, setEditingService] = useState(null);
    const [myListings, setMyListings] = useState([]);
    const [user, setUser] = useState(null);

    useEffect(() => {
        const fetchUserListings = async () => {
            try {
                // Check if the user is authenticated
                const loggedUserJSON = window.localStorage.getItem('loggedServiceAppUser');
                if (!loggedUserJSON) {
                    // Redirect the user to the login page or perform necessary actions
                    // For example, you can use React Router to navigate to the login page
                    return;
                }

                const loggedUser = JSON.parse(loggedUserJSON);
                setUser(loggedUser);
                // When user refreshes, we must set the token
                serviceService.setToken(loggedUser.token);
                // Fetch services owned by the user
                const listings = await serviceService.getMyListings();
                setMyListings(listings);
            } catch (error) {
                console.error('Error fetching user listings:', error);
            }
        };

        fetchUserListings();
    }, []);      
      
    const handleEdit = (service) => {
        setEditingService(service);
    };

    const handleCancelEdit = () => {
        setEditingService(null);
    };

    const handleUpdateService = async (updatedService) => {
        try {
            // Implement logic to update the service using the serviceService
            await serviceService.update(updatedService);
            // After updating, refresh the service list or update state
            const updatedListings = myListings.map((listing) =>
                listing._id === updatedService._id ? updatedService : listing
            );
            setMyListings(updatedListings);
            // Close the edit form
            setEditingService(null);
        } catch (error) {
            console.error('Error updating service:', error);
        }
    };

    const handleDeleteService = async (service) => {
        try {
            // Ask for confirmation before proceeding
            const isConfirmed = window.confirm("Are you sure you want to delete this service listing?");
            
            if (!isConfirmed) {
                return; // If not confirmed, do nothing
            }

            // We need to access the ID of the service object
            const serviceId = service._id;
            
            // Implement logic to delete the service using the serviceService
            await serviceService.remove(serviceId);
            
            // After deleting, refresh the service list or update state
            const updatedListings = myListings.filter((listing) => listing._id !== serviceId);
            setMyListings(updatedListings);
        } catch (error) {
            console.error('Error deleting service:', error);
        }
    };

    return (
        <div>
            <h2>My Profile</h2>
            {user && (
              <div>
                <p>Username: {user.username}</p>
                <p>Name: {user.name}</p>
              </div>
                )}
            {!myListings || myListings.length === 0 ? (
                <p>No listings found.</p>
            ) : (
                <ul>
                    <h3>My Listings</h3>
                    {myListings.map((listing) => (
                        <li key={listing._id}>
                            {editingService && editingService._id === listing._id ? (
                                <EditServiceForm
                                    service={editingService}
                                    onCancel={handleCancelEdit}
                                    onUpdate={handleUpdateService}
                                />
                            ) : (
                                <>
                                    {listing.name}
                                    <button onClick={() => handleEdit(listing)}>Edit</button>
                                    <button onClick={() => handleDeleteService(listing)}>Delete</button>
                                </>
                            )}
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default MyProfile;
