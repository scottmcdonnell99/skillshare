// SearchInput.jsx
import React from 'react';
import '../css/AllServices.css';
 
const SearchInput = ({ value, onChange, placeholder }) => {
  return (
    <input
      type="text"
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      className="search-bar"
    />
  );
};

export default SearchInput;
