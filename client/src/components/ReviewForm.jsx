import React, { useState } from 'react';
import serviceService from '../services/services';
import '../App.css'

const ReviewForm = ({ serviceId, onReviewSubmit }) => {
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState('');

  const handleRatingChange = (event) => {
    setRating(event.target.value);
  };

  const handleCommentChange = (event) => {
    setComment(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      // Submit the review
      const newReview = { rating, comment };
      await serviceService.addReviewForService(serviceId, newReview);

      // Clear the form
      setRating(0);
      setComment('');

      // Notify parent component to update reviews
      onReviewSubmit();
    } catch (error) {
      console.error('Error submitting review:', error);
      // Handle error, show a message, etc.
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Rating:
        <select value={rating} onChange={handleRatingChange}>
          <option value={0}>Select Rating</option>
          <option value={1}>1</option>
          <option value={2}>2</option>
          <option value={3}>3</option>
          <option value={4}>4</option>
          <option value={5}>5</option>
        </select>
      </label>
      <br />
      <label>
        Comment:
        <textarea value={comment} onChange={handleCommentChange} />
      </label>
      <br />
      <button type="submit">Submit Review</button>
    </form>
  );
};

export default ReviewForm;
