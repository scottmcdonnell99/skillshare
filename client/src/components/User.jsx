// User.jsx
import React from 'react';
import { Link } from 'react-router-dom';
import '../App.css'

const User = ({ user }) => {
  return (
    <li key={user._id}>
      <Link to={`/users/${user._id}`}>{user.username}</Link>
    </li>
  );
};

export default User;
