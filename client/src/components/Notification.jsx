// Notification.js
import React from 'react';
import '../App.css'

const Notification = ({ notification }) => {
  return (
    <div className={`notification ${notification.read ? 'read' : 'unread'}`}>
      {notification.message}
    </div>
  );
};

export default Notification;
