import React from 'react';
import { Link } from 'react-router-dom';

const NavButtons = () => {
  return (
    <div className="button-container">
      <div className="spacer"></div>
      <button className="nav-button">
        <Link to={`/`}>Home</Link>
      </button>
      <button className="profile-button">
        <Link to={`/my-profile`}>My Profile</Link>
      </button>

      <button className="listings-button">
        <Link to={`/my-listings`}>My Listings</Link>
      </button>

      <button className="bookmarks-button">
        <Link to={`/bookmarks`}>Bookmarks</Link>
      </button>
    </div>
  );
};

export default NavButtons;
