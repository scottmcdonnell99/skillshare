import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

const Togglable = forwardRef((props, ref) => {
  const [visible, setVisible] = useState(false);

  const hideWhenVisible = { display: visible ? 'none' : '' };
  const showWhenVisible = { display: visible ? '' : 'none' };

  const toggleVisibility = () => {
    setVisible(!visible);
  };

  useImperativeHandle(ref, () => {
    return {
      toggleVisibility,
    };
  });

  return (
    <div style={{ position: 'relative' }}>
      <div style={hideWhenVisible}>
        <button className={props.buttonClassName} onClick={toggleVisibility}>
          <span>{props.buttonLabel}</span>
        </button>
      </div>
      <div style={showWhenVisible}>
        <div>{props.children}</div>
        <button 
          onClick={toggleVisibility} 
          style={{
            position: 'relative',
            top: '0',
            right: '0',
          }}
        >
          <FontAwesomeIcon icon={faTimes} />
        </button>
      </div>
    </div>
  );
});

export default Togglable;
