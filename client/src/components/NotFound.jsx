// NotFound.js
import React from 'react';
import '../App.css'

const NotFound = () => {
  return <h2>404 Not Found</h2>;
};

export default NotFound;
