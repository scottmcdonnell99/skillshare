// DropdownMenu.jsx

import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const DropdownMenu = ({ user, handleLogout }) => {
  const [isDropdownOpen, setDropdownOpen] = useState(false);

  const toggleDropdown = () => {
    setDropdownOpen(!isDropdownOpen);
  };

  const handleLogoutClick = (event) => {
    event.preventDefault();
    handleLogout(); // Call the handleLogout function after preventing default behavior
  };

  return (
    <div className="dropdown-container">
      <button
        onClick={toggleDropdown}
        className={`nav-button dropbtn ${isDropdownOpen ? 'open' : ''}`}
        aria-expanded={isDropdownOpen}
      >
        <span>Welcome, {user.name}!</span>
      </button>
      {isDropdownOpen && (
        <div className="dropdown-content down">
          <button className="dropdown-button">
            <Link to={`/my-profile`}>
              My Profile
            </Link>
          </button>
          <button className="dropdown-button">
            <Link to={`/my-listings`}>
              My Listings
            </Link>
          </button>
          <button className="dropdown-button">
            <Link to={`/bookmarks`}>
              Bookmarks
            </Link>
          </button>
          <button className="dropdown-button" onClick={handleLogoutClick}>
            Logout
          </button>
        </div>
      )}
    </div>
  );
};

export default DropdownMenu;
