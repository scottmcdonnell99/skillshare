import React from 'react';
import '../App.css'

const Filter = ({ filters, setFilters }) => {
  return (
    <div>
      <h2>Filter Services</h2>
      <label htmlFor="filterName">Name:</label>
      <input
        type="text"
        id="filterName"
        name="name"
        value={filters.name}
        onChange={(e) => setFilters({ ...filters, name: e.target.value })}
      />

      <label htmlFor="filter$">Category:</label>
      <input
        type="text"
        id="filterCategory"
        name="category"
        value={filters.category}
        onChange={(e) => setFilters({ ...filters, category: e.target.value })}
      />

      <label htmlFor="filterLocation">Location:</label>
      <input
        type="text"
        id="filterLocation"
        name="location"
        value={filters.location}
        onChange={(e) => setFilters({ ...filters, location: e.target.value })}
      />

      <label htmlFor="filterPhone">Phone:</label>
      <input
        type="text"
        id="filterPhone"
        name="phone"
        value={filters.phone}
        onChange={(e) => setFilters({ ...filters, phone: e.target.value })}
      />
      <label htmlFor="filterPrice">Price:</label>
      <input
        type="text"
        id="filterPrice"
        name="price"
        value={filters.price}
        onChange={(e) => setFilters({ ...filters, price: e.target.value })}
      />

    </div>
  );
};

export default Filter;
