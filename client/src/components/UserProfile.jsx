// UserProfile.jsx
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import userService from '../services/users';
import '../App.css'

const UserProfile = () => {
  const { userId } = useParams();
  const [user, setUser] = useState(null);

  useEffect(() => {
    // Ensure userId is defined before making the API call
    if (userId) {
      // Fetch user data
      userService.getUserById(userId)
        .then((userData) => setUser(userData))
        .catch((error) => console.error('Error fetching user data:', error));
    }
  }, [userId]);

  if (!user) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <h2>User Profile</h2>
      <p>Username: {user.username}</p>
      <p>Name: {user.name}</p>

      <h3>Services Posted by {user.username}</h3>
      {user.services && user.services.length === 0 ? (
        <p>No services posted by this user.</p>
      ) : (
        <ul>
          {user.services.map((service) => (
          <li key={service._id}>
          <Link to={`/services/${service._id}`}>
            {service.name}
          </Link>
          </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default UserProfile;
