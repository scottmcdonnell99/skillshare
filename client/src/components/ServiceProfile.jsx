import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import serviceService from '../services/services';
import '../App.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as solidHeart } from '@fortawesome/free-solid-svg-icons';
import { faHeart as emptyHeart } from '@fortawesome/free-regular-svg-icons';
import ReviewForm from './ReviewForm';

const ServiceProfile = ({ user }) => {
  const { serviceId } = useParams();
  const [service, setService] = useState(null);
  const [reviews, setReviews] = useState([]);
  const [views, setViews] = useState(0);
  const [isBookmarked, setIsBookmarked] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch service data
        const serviceData = await serviceService.getServiceById(serviceId);
        setService(serviceData);

        // Fetch reviews for the service
        const reviewsData = await serviceService.getReviewsForService(serviceId);
        setReviews(reviewsData);

        // Fetch views for the service
        const viewsData = await serviceService.incrementServiceViews(serviceId);
        setViews(viewsData.views);

        const bookmarksData = await serviceService.getBookmarksForService(serviceId);
        setIsBookmarked(bookmarksData.isBookmarked);

      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [serviceId]);
  

  const handleReviewSubmit = async () => {
    try {
      // Refresh reviews after a new review is submitted
      const reviewsData = await serviceService.getReviewsForService(serviceId);
      setReviews(reviewsData);
    } catch (error) {
      console.error('Error fetching reviews:', error);
    }
  };

  const handleToggleBookmark = async () => {
    try {
      const response = await serviceService.toggleBookmark(serviceId);
  
      setIsBookmarked((prevIsBookmarked) => {
        const newIsBookmarked = !prevIsBookmarked;
        return newIsBookmarked;
      });
  
      // Perform additional actions based on the response, if needed
    } catch (error) {
      // Handle the error, show an error message, etc.
      console.error('Error toggling bookmark:', error.message);
    }
  };

  const renderBookmarkButton = () => {
    if (!isOwner && user) {
      return (
        <button
          onClick={handleToggleBookmark}
          style={{ 
            color: isBookmarked ? 'red' : 'inherit',
            backgroundColor: 'transparent', // Set background color to transparent
            border: 'none', // Remove button border
            outline: 'none', // Remove button outline
            fontSize: '20px', // Adjust the size as needed
          }}        >
          <FontAwesomeIcon icon={isBookmarked ? solidHeart : emptyHeart} />
        </button>
      );
    }
    return null;
  };
  
  if (!service) {
    return <p>Loading...</p>;
  }

  const isOwner = user && service.userId.username === user.username;

  return (
    <div>
      <h2>{service.name}</h2>
      <p>Category: {service.category}</p>
      <p>Location: {service.location}</p>
      <p>Phone: {service.phone}</p>
      <p>Price: {service.price}</p>
      <p>Posted by: {service.userId.username}</p>
      <p>Average Rating: {service.averageRating}</p>
      <p>Views: {service.views}</p>
      {renderBookmarkButton()}
      <h3>Reviews:</h3>
      {reviews.length === 0 ? (
        <p>No reviews yet.</p>
      ) : (
        <ul>
          {reviews.map((review) => (
            <li key={review._id}>
              <p>User: {review.user.name}</p>
              <p>Rating: {review.rating}</p>
              <p>Comment: {review.comment}</p>
            </li>
          ))}
        </ul>
      )}
      {/* Conditionally render the ReviewForm based on ownership */}
      {!isOwner && <ReviewForm serviceId={serviceId} onReviewSubmit={handleReviewSubmit} />}
    </div>
  );
};

export default ServiceProfile;
