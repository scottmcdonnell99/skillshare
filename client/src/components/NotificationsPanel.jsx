// Notifications.jsx
import React from 'react';
import '../App.css'

const NotificationsPanel = ({ notifications }) => {
  return (
    <div>
      <h3>Notifications</h3>
      <ul>
        {notifications.map((notification, index) => (
          <li key={index}>{notification}</li>
        ))}
      </ul>
    </div>
  );
};

export default NotificationsPanel;
