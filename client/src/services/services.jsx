import axios from 'axios';
import mapService from './map'
const baseUrl = 'http://localhost:3000/api/services';

let token = null;

const setToken = newToken => {
  token = `Bearer ${newToken}`;
};

const getAll = async () => {
  try {
    const response = await axios.get(baseUrl);
    console.log(response.data)
    return response.data;
  } catch (error) {
    console.error('Error fetching data:', error.message);
    throw error;
  }
};

const getServiceById = async (serviceId) => {
    try {
      const response = await axios.get(`${baseUrl}/${serviceId}`);
      return response.data;
    } catch (error) {
      console.error('Error fetching serrvice by ID:', error.message);
      throw error;
    }
  };

const create = async newObject => {
  try {
    const config = {
      headers: { Authorization: token },
    };

    const response = await axios.post(baseUrl, newObject, config);
    return response.data;
  } catch (error) {
    console.error('Error creating object:', error.message);
    throw error;
  }
};

const update = async (updatedService) => {
  try {
    const response = await axios.put(`${baseUrl}/${updatedService._id}`, updatedService, {
      headers: { Authorization: token },
    });
    return response.data;
  } catch (error) {
    console.error('Error updating service:', error.message);
    throw error;
  }
};

const remove = async (serviceId) => {
  try {
    const response = await axios.delete(`${baseUrl}/${serviceId}`, {
      headers: { Authorization: token },
    });
    return response.data;
  } catch (error) {
    console.error('Error deleting service:', error.message);
    throw error;
  }
};


const getMyListings = async () => {
  try {
    const response = await axios.get(`${baseUrl}/my-listings`, {
      headers: { Authorization: token },
    });
    return response.data;
  } catch (error) {
    console.error('Error fetching user listings:', error.message);
    throw error;
  }
};

const addReviewForService = async (serviceId, reviewData) => {
  try {
    const response = await axios.post(`${baseUrl}/${serviceId}/reviews`, reviewData, {
      headers: { Authorization: token },
    });
    return response.data;
  } catch (error) {
    console.error('Error adding review:', error.message);
    throw error;
  }
};

const getReviewsForService = async (serviceId) => {
  try {
    const response = await axios.get(`${baseUrl}/${serviceId}/reviews`);
    return response.data;
  } catch (error) {
    console.error('Error fetching reviews:', error.message);
    throw error;
  }
};

const incrementServiceViews = async (serviceId) => {
  try {
    const response = await axios.post(`${baseUrl}/${serviceId}/increment-views`, null, {
      headers: { Authorization: token },
    });
    return response.data.views;
  } catch (error) {
    console.error('Error incrementing views:', error.message);
    throw error;
  }
}

const toggleBookmark = async (serviceId) => {
  try {
    const config = {
      headers: { Authorization: token },
    };

    const url = `${baseUrl}/${serviceId}/bookmark`;
    const response = await axios.post(url, null, config);

    return response.data;
  } catch (error) {
    console.error('Error toggling bookmark:', error.message);
    throw error;
  }
};

const getBookmarksForService = async (serviceId) => {
  try {
    // Fetch service data, including bookmarks
    const config = {
      headers: { Authorization: token },
    };
    const url = `${baseUrl}/${serviceId}/bookmark-status`;

    const response = await axios.get(url, config);
    const { isBookmarked } = response.data;
    return { isBookmarked };
  } catch (error) {
    console.error('Error fetching bookmarks for service:', error.message);
    throw error;
  }
};

const geocodeAddress = async (address) => {
  try {

    // Fetch API key dynamically from the backend
    const apiKey = await mapService.fetchApiKey();


    // Make geocoding request
    const response = await axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
      params: {
        address: address,
        key: apiKey,
      },
    });


    // Check for valid results
    if (!response.data.results || response.data.results.length === 0) {
      console.warn(`Geocoding failed for address "${address}". No results found.`);
      return null; // or return an empty object: {}
    }

    const result = response.data.results[0];
    const location = result.geometry.location;

    return { lat: location.lat, lng: location.lng };
  } catch (error) {
    console.error(`Error geocoding address "${address}":`, error.message);
    throw error;
  }
};


// Explicitly export functions and variables
// eslint-disable-next-line
export default { setToken, getAll, getServiceById, create, update, remove, getMyListings, addReviewForService, getReviewsForService, incrementServiceViews, toggleBookmark, getBookmarksForService, geocodeAddress};
