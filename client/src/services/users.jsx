// userService.js
import axios from 'axios';

const baseUrl = 'http://localhost:3000/api/users';

let token = null;

const setToken = newToken => {
  token = `Bearer ${newToken}`;
};

const getAll = async () => {
  try {
    const response = await axios.get(baseUrl);
    return response.data;
  } catch (error) {
    console.error('Error fetching users:', error.message);
    throw error;
  }
};

const getUserById = async (userId) => {
  try {
    const response = await axios.get(`${baseUrl}/${userId}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching user by ID:', error.message);
    throw error;
  }
};

const getUserBookmarks = async (userId) => {
  try {
    const url = `${baseUrl}/bookmarks/${userId}`;

    const response = await axios.get(url, {
      headers: { Authorization: token },
    });
    
    return response.data;
  } catch (error) {
    if (error.response) {
      console.error('Error Details:', error.response.data);
      if (error.response.status === 404) {
        console.error('Resource not found.');
      }
    } else if (error.request) {
      console.error('No response received for the request.');
    } else {
      console.error('Error setting up the request:', error.message);
    }

    // Return a default value or rethrow the error based on your needs
    return [];
    // throw error;
  }
};




const userService = {
  setToken,
  getAll,
  getUserById,
  getUserBookmarks,
};

export default userService;
