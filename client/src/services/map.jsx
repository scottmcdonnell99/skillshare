// mapService.js
import axios from 'axios';

const baseUrl = 'http://localhost:3000/api';

const fetchApiKey = async () => {
  try {
    const response = await axios.get(`${baseUrl}/api-key`);
    const data = response.data;
    return data.key;
  } catch (error) {
    console.error('Error fetching API key:', error.message);
    throw error;
  }
};

const mapService = {
  fetchApiKey,
};

export default mapService;
