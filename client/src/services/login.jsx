import axios from 'axios';
import { jwtDecode } from "jwt-decode";


const baseUrl = 'http://localhost:3000/api/login'

const isTokenExpired = (token) => {
  if (!token) {
    return true;
  }

  const decodedToken = jwtDecode(token);
  return decodedToken.exp * 1000 < Date.now();
};

const login = async credentials => {
  try {
    const response = await axios.post(baseUrl, credentials)
    return response.data
  } catch (error) {
    // Handle errors, log them, or throw a specific error
    console.error('Login service error:', error)
    throw error
  }
}

// eslint-disable-next-line
export default { login, isTokenExpired }