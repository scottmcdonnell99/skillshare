// registrationService.jsx
import axios from 'axios';

const baseUrl = 'http://localhost:3000/api/register';

const register = async (userData) => {
  try {
    const response = await axios.post(baseUrl, userData);
    return response.data;
  } catch (error) {
    console.error('Registration service error:', error);
    throw error;
  }
};

export default register ;
