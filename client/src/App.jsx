import React, { useState, useEffect, useRef } from 'react';
import { Routes, Route, Link, useNavigate } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/Home';
import NotFound from './components/NotFound';
import UserProfile from './components/UserProfile';
import ServiceProfile from './components/ServiceProfile';
import AllServices from './components/AllServices';
import AllUsers from './components/AllUsers';
import MyListings from './components/MyListings';
import MyProfile from './components/MyProfile';
import Bookmarks from './components/Bookmarks'
import serviceService from './services/services';
import loginService from './services/login';
import userService from './services/users';
import './App.css';

const App = () => {
  const [services, setServices] = useState([]);
  const [users, setUsers] = useState([]);
  const [newService, setNewService] = useState({
    name: '',
    category: '',
    location: '',
    phone: '',
    price: '',
  });
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');
  const serviceFormRef = useRef();
  const [userListVisible, setUserListVisible] = useState(false);
  const [userFilters, setUserFilters] = useState({
    search: '',
  });
  const [successMessage, setSuccessMessage] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    serviceService
      .getAll()
      .then((initialServices) => {
        setServices(initialServices);
      })
      .catch((error) => {
        console.error('Error fetching initial services:', error);
        setErrorMessage('Error fetching services. Please try again.');
      });
  }, []);

  useEffect(() => {
    userService
      .getAll()
      .then((initialUsers) => {
        setUsers(initialUsers);
      })
      .catch((error) => {
        console.error('Error fetching initial users:', error);
        setErrorMessage('Error fetching users. Please try again.');
      });
  }, []);

  useEffect(() => {
    const loggedUserJSON = window.localStorage.getItem('loggedServiceAppUser');
    if (loggedUserJSON) {
      const user = JSON.parse(loggedUserJSON);
      setUser(user);
      if (user.token) {
        if (loginService.isTokenExpired(user.token)) {
          handleLogout();
      } else {
          serviceService.setToken(user.token);
        }
      }
    }
  }, []);

  

const handleLogin = async (event) => {
  event.preventDefault();

  try {
    const loggedInUser = await loginService.login({
      username,
      password,
    });

    window.localStorage.setItem('loggedServiceAppUser', JSON.stringify(loggedInUser));
    serviceService.setToken(loggedInUser.token);
    setUsername('');
    setPassword('');
    setSuccessMessage('Login successful!');
    navigate('/')
    window.location.reload();
  } catch (exception) {
    if (exception.response && exception.response.status === 401) {
      if (exception.response.data.error === 'jwt expired') {
        // Handle expired token, e.g., redirect to login page or show a message
        navigate('/')
        window.location.reload();
      } else {
        setErrorMessage('Wrong credentials');
        setTimeout(() => {
          setErrorMessage('');
        }, 5000);
      }
    } else {
      // Handle other errors
      console.error('Error during login:', exception);
    }
  }
};


  const handleServiceChange = (event) => {
    setNewService({ ...newService, [event.target.name]: event.target.value });
  };

  const handleAddService = (serviceObject) => {
    serviceService
      .create(serviceObject)
      .then((returnedService) => {
        setServices([...services, returnedService]);
        serviceFormRef.current.toggleVisibility();
        setSuccessMessage('Successful added a service!');
      })
      .catch((error) => {
        console.error('Error creating service:', error);
        setErrorMessage('Failed to create service. Please try again.');
        setTimeout(() => {
          setErrorMessage('');
        }, 5000);
      });
  };

  const handleLogout = () => {
    console.log('logout triggered')
    localStorage.removeItem('loggedServiceAppUser');
    serviceService.setToken(null);
    navigate('/')
    window.location.reload();
  }
  
  const filteredUsers = users.filter((user) => {
    return (
      user.username.includes(userFilters.search) ||
      user.name.includes(userFilters.search)
    );
  });

  const updateServicesAfterDeletion = (deletedServiceId) => {
    setServices((prevServices) =>
      prevServices.filter((service) => service._id !== deletedServiceId)
    );
  };

  return (
    <div>
      <Navbar user={user} 
      handleLogout={handleLogout} 
      handleLogin={handleLogin}
      handleAddService={handleAddService}
      handleServiceChange={handleServiceChange}
      newService={newService}
      serviceFormRef={serviceFormRef}
      setUsername={setUsername}
      setPassword={setPassword}
      username={username}
      password={password}
      />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/services" element={<AllServices />} />
        <Route path="/users" element={<AllUsers />} />
        <Route
          path="/services/:serviceId"
          element={<ServiceProfile user={user} />}
        />
        <Route path="/users/:userId" element={<UserProfile />} />
        <Route
          path="/my-listings"
          element={<MyListings onDeleteService={updateServicesAfterDeletion} />}
        />
        <Route path="/my-profile" element={<MyProfile user={user} />} />
        <Route path="/bookmarks" element={<Bookmarks />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
      {errorMessage && <p className="error-message">{errorMessage}</p>}
      {successMessage && <p className="success-message">{successMessage}</p>}
      <AllServices user={user}/>
      <div>
        <button
          onClick={() => setUserListVisible(!userListVisible)}
        >
          {userListVisible ? 'Hide Users' : 'Show Users'}
        </button>
        {userListVisible && (
          <ul>
            <input
              type="text"
              placeholder="Search Users"
              value={userFilters.search}
              onChange={(e) => setUserFilters({ ...userFilters, search: e.target.value })}
            />
            {filteredUsers.map((user) => (
              <li key={user._id}>
                <Link to={`/users/${user._id}`}>
                  {user.username}
                </Link>
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  )}  
export default App
